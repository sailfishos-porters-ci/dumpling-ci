# Sailfish OS on OnePlus 5T

* 3.4.0.24 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/dumpling-ci/badges/3.4.0.24/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/dumpling-ci/commits/3.4.0.24)
* 3.3.0.16 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/dumpling-ci/badges/3.3.0.16/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/dumpling-ci/commits/3.3.0.16)
* 3.2.1.20 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/dumpling-ci/badges/3.2.1.20/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/dumpling-ci/commits/3.2.1.20)

This repository uses [GitLab's Continuous Integration](https://docs.gitlab.com/ee/ci) to build Sailfish OS images for the OnePlus 5T.

![OnePlus 5](https://wiki.merproject.org/w/images/c/c0/Sailfish-OnePlus5.png "OnePlus 5")

## Device specifications

Part         | Specification
------------:|:-------------
Chipset      | Qualcomm Snapdragon 835 (MSM8998)
CPU          | Octa-Core Kryo 280 (4x 2.45 GHz Cortex A73 & 4x 1.90 GHz Cortex A53)
Arch         | ARMv8-A 64-bit / aarch64 / arm64
GPU          | Qualcomm Adreno 540
RAM          | 6 or 8 GB LPDDR4X
Storage      | 64 or 128 GB UFS 2.1
Android      | 7.1.1, up to 10.0 (OxygenOS)
Kernel       | Linux 4.4.x series
Battery      | 3 300 mAh Li-Po
Display      | 1080 x 2160 pixels, 6" Optic AMOLED
Rear Cameras | 16 MP (Sony Exmor IMX398) + 20 MP (Sony Exmor IMX376K)
Front Camera | 16 MP (Sony Exmor IMX371)

A bunch of the other specifics can be found [here](https://www.gsmarena.com/oneplus_5t-8912.php).

# Files

[dumpling.env](dumpling.env) contains all the environment variables required for the build process. The [`RELEASE` variable](dumpling.env#L6) is changed on every release in the `master` branch and a new tag is created with the new version to trigger the build process.

[Jolla-@RELEASE@-dumpling-@ARCH@.ks](Jolla-@RELEASE@-dumpling-@ARCH@.ks) is the kickstart file used by the PlatformSDK image. The file contains device specific repositories, which can be changed by from `devel` to `testing` or vice versa.

[run-mic.sh](run-mic.sh) is a simple bash script which executes the `mic` build.

# Download

[Download the latest build](https://gitlab.com/sailfishos-porters-ci/dumpling-ci/-/jobs?scope=finished)

# Source code

[https://github.com/sailfishos-oneplus5](https://github.com/sailfishos-oneplus5)


This README doc format is from @0312birdzhang and the [vince-ci](https://gitlab.com/sailfishos-porters-ci/vince-ci), thank you! ^^
